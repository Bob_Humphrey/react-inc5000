import React, { useContext } from "react";
import { AppContext } from "../contexts/AppContext";
import SortButton from "./SortButton";

const FilterGrowth = () => {
  const appContext = useContext(AppContext);
  const {
    endpointParms,
    showFilterGrowth,
    handleFilterGrowth,
    clearFilterGrowth,
    toggleFilterGrowth
  } = appContext;
  const showGrowthInput =
    endpointParms.growth === 0 ? "" : endpointParms.growth;
  const showClear =
    endpointParms.growth === 0 || endpointParms.growth === "0" ? "hidden" : "";
  const showFilter = showFilterGrowth ? "" : "hidden";
  const statusIndicator = showFilterGrowth ? "- " : "+ ";
  const filterColor = endpointParms.sort === "growth" ? "text-green-600" : "";

  return (
    <section className="bg-white font-inter_semibold border border-gray-200 rounded mb-1 mx-5 p-3">
      {/* FILTER HEADER */}
      <div className="flex">
        <div className="flex w-1/6 text-center ">
          <SortButton sortField="growth" sortDirection="asc" />
          <SortButton sortField="growth" sortDirection="desc" />
        </div>
        <div
          className="flex w-5/6 justify-between cursor-pointer "
          onClick={() => toggleFilterGrowth()}
        >
          <div className={filterColor + " justify-start hover:text-green-600"}>
            {statusIndicator + " "}Growth (Percent) >
          </div>
          <div className="justify-start text-gray-500">
            {endpointParms.growth}
          </div>
        </div>
      </div>

      {/* FILTER TEXTBOX AND BUTTONS */}
      <div className={showFilter + " flex justify-center mt-3"}>
        <div className="w-5/6 grid grid-cols-3 bg-gray-200 rounded">
          <div className="py-3 text-center">Growth ></div>
          <div className="py-2 ">
            <input
              className="w-full py-1 px-1 border border-gray-200 rounded"
              type="text"
              name="heading"
              value={showGrowthInput}
              onChange={handleFilterGrowth}
            />
          </div>
          <div className="py-2">
            <button
              className={
                showClear +
                " font-inter_semibold text-gray-600 px-4 py-1 mx-2 focus:outline-none rounded"
              }
              onClick={() => clearFilterGrowth()}
            >
              Clear
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FilterGrowth;
