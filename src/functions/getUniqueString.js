const getUniqueString = () => {
  let uniqueString =
    Math.random()
      .toString(36)
      .substring(2, 15) +
    Math.random()
      .toString(36)
      .substring(2, 15);
  return uniqueString;
};

export default getUniqueString;
